package com.nailgun.study.algo.extractor;

import java.util.List;

public interface IntListExtractor {
	
	List<Integer> extract();

}
