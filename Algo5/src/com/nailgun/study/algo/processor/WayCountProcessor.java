package com.nailgun.study.algo.processor;

import com.google.common.collect.Table;

public interface WayCountProcessor {
	
	int count(Table<Integer, Integer, Integer> data, Integer start, Integer finish);

}
