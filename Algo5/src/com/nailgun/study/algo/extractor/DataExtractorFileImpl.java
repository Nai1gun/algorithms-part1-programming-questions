package com.nailgun.study.algo.extractor;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;

public class DataExtractorFileImpl implements DataExtractor {

	@Override
	public Table<Integer, Integer, Integer> extract() {
		Table<Integer, Integer, Integer> ret = HashBasedTable.create();
		InputStream arrayStream = DataExtractorFileImpl.class.getResourceAsStream("dijkstraData.txt");
		try{
			List<String> strLines = IOUtils.readLines(arrayStream);
			System.out.println("Lines size: " + strLines.size());
			int num = 1;
			for (String line: strLines){
				List<String> splitted = Lists.newArrayList(StringUtils.split(line, "\t"));
				for (int i = 1; i < splitted.size(); i++){
					String verticeStr = splitted.get(i);
					Integer head = Integer.valueOf(StringUtils.substringBefore(verticeStr, ","));
					Integer weight = Integer.valueOf(StringUtils.substringAfter(verticeStr, ","));
					ret.put(num, head, weight);
				}
				num++;
			}
			return ret;
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			IOUtils.closeQuietly(arrayStream);
		}
		return null;
	}

}
