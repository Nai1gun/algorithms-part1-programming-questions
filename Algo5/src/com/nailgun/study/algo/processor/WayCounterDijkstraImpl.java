package com.nailgun.study.algo.processor;

import java.util.Map;
import java.util.Set;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;

public class WayCounterDijkstraImpl implements WayCountProcessor {	

	private Table<Integer, Integer, Integer> data;
	private Integer start, finish;
	private Map<Integer, Integer> shortestPath;
	private Set<Integer> processed;
	private Set<Integer> notProcessed;

	@Override
	public int count(Table<Integer, Integer, Integer> data, Integer start, Integer finish) {
		this.data = HashBasedTable.create(data);
		this.start = start;
		this.finish = finish;
		shortestPath = Maps.newHashMapWithExpectedSize(data.rowKeySet().size());
		processed = Sets.newHashSet();
		notProcessed = Sets.newHashSet(data.rowKeySet());
		notProcessed.addAll(data.columnKeySet());
		return doCount();
	}
	
	private int doCount(){
		notProcessed.remove(start);
		processed.add(start);
		shortestPath.put(start, 0);
		while (!processed.contains(finish)){
			Integer minPath = null;
			Integer nodeToProcess = null;
			Table<Integer, Integer, Integer> crossing = getCrossing();
			for (Table.Cell<Integer, Integer, Integer> cell: crossing.cellSet()){
				Integer pathToTail = shortestPath.get(cell.getRowKey());
				Integer pathToHead = pathToTail + cell.getValue();
				if (minPath == null || minPath > pathToHead){
					minPath = pathToHead;
					nodeToProcess = cell.getColumnKey();
				}
			}
			if (minPath == null || nodeToProcess == null){
				throw new RuntimeException("No path?");
			}
			processed.add(nodeToProcess);
			notProcessed.remove(nodeToProcess);
			shortestPath.put(nodeToProcess, minPath);
			
		}		
		return shortestPath.get(finish);
	}
	
	private Table<Integer, Integer, Integer> getCrossing(){
		Table<Integer, Integer, Integer> ret  = HashBasedTable.create();
		for (Integer tail: data.rowKeySet()){
			for(Integer head: data.row(tail).keySet()){
				Integer value = data.get(tail, head);
				if (value != null){
					if (processed.contains(tail) && notProcessed.contains(head)){
						ret.put(tail, head, data.get(tail, head));
					//since we have undirected graph
					} else if (processed.contains(head) && notProcessed.contains(tail)){
						ret.put(head, tail, data.get(tail, head));
					}
				}
			}
		}
		return ret;
	}
	
	private void makeData(Table<Integer, Integer, Integer> input){
		Table<Integer, Integer, Integer> data = HashBasedTable.create();
		for (Table.Cell<Integer, Integer, Integer> cell: input.cellSet()){
			if (!data.contains(cell.getRowKey(), cell.getColumnKey()) && 
					!data.contains(cell.getRowKey(), cell.getColumnKey())){
				data.put(cell.getRowKey(), cell.getColumnKey(), cell.getValue());
			}				
		}
	}

}
