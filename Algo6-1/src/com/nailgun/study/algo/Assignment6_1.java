package com.nailgun.study.algo;

import java.util.ArrayList;
import java.util.List;

import com.nailgun.study.algo.counter.TwoSumCounter;
import com.nailgun.study.algo.counter.TwoSumCounterHashSetImpl;
import com.nailgun.study.algo.extractor.IntListExtractor;
import com.nailgun.study.algo.extractor.IntListExtractorFileImpl;

public class Assignment6_1 {
	
	private static final List<Integer> RANGE = new ArrayList<Integer>(1501);
	
	static{
		for (Integer i = 2500; i <= 4000; i++){
			RANGE.add(i);
		}
	}
	
	public static void main(String[] args) {
		IntListExtractor extractor = new IntListExtractorFileImpl();
		List<Integer> ints = extractor.extract();
		TwoSumCounter counter = new TwoSumCounterHashSetImpl(ints);
		int result = 0;
		for (Integer t: RANGE){
			if (counter.count(t) > 0) {
				result++;
				System.out.println("Matches for: " + t);
			}
		}
		System.out.println("Result: " + result);
	}

}
