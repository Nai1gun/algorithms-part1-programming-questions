package com.nailgun.study.algo;

import java.util.List;

import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import com.nailgun.study.algo.extractor.DataExtractor;
import com.nailgun.study.algo.extractor.DataExtractorFileImpl;
import com.nailgun.study.algo.extractor.DataExtractorStubImpl;
import com.nailgun.study.algo.processor.WayCountProcessor;
import com.nailgun.study.algo.processor.WayCounterDijkstraImpl;

public class Assignment5 {
	
	private static final List<Integer> finishNodes = Lists.newArrayList(
		7,37,59,82,99,115,133,165,188,197);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DataExtractor extractor = new DataExtractorFileImpl();
		Table<Integer, Integer, Integer> data = extractor.extract();
		WayCountProcessor processor = new WayCounterDijkstraImpl();
		String result = "";
		for(Integer finish: finishNodes){
			int counted = processor.count(data, 1, finish);
			result += ",";
			result += counted;
		}
		System.out.println(result);
	}

}
