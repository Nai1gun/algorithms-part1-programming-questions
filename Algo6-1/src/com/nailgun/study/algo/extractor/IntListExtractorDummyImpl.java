package com.nailgun.study.algo.extractor;

import java.util.Arrays;
import java.util.List;

public class IntListExtractorDummyImpl implements IntListExtractor{
	
	public List<Integer> extract(){
		List<Integer> ints;
		Integer[] arr = new Integer[]{
			500, 1000, 1800, 1800, 2000, 2000, 3000, 4000
		};
		ints = Arrays.asList(arr);
		return ints;
	}

}
