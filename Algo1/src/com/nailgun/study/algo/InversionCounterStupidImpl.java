package com.nailgun.study.algo;

import java.util.List;

public class InversionCounterStupidImpl implements InversionCounter {

	@Override
	public long count(List<Integer> list) {
		long count = 0l;
		for (int i = 0; i < list.size(); i++){
			int current = list.get(i);
			for (int j = i; j < list.size(); j++){
				if (list.get(j) < current)
					count ++;
			}
		}
		return count;
	}

}
