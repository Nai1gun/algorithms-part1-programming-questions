package com.nailgun.study.algo.extractor;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

public class IntListExtractorFileImpl implements IntListExtractor{
	
	public List<Integer> extract(){
		InputStream arrayStream = IntListExtractorFileImpl.class.getResourceAsStream("HashInt.txt");
		try {
			List<String> strLines = IOUtils.readLines(arrayStream);
			System.out.println("Lines size: " + strLines.size());
			List<Integer> ints = new ArrayList<Integer>(strLines.size());
			for (String str: strLines){
				int number = Integer.parseInt(str);
				ints.add(number);
			}
			return ints;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally{
			IOUtils.closeQuietly(arrayStream);
		}
	}

}
