package com.nailgun.study.algo;

import java.util.List;

public interface InversionCounter {
	long count(List<Integer> list);
}
