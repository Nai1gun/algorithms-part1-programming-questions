package com.nailgun.study.algo.counter;

public interface TwoSumCounter {
	
	int count(Integer t);

}
