package com.nailgun.study.algo.extractor;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

public class DataExtractorStubImpl implements DataExtractor {

	@Override
	public Table<Integer, Integer, Integer> extract() {
		Table<Integer, Integer, Integer> ret = HashBasedTable.create();
		ret.put(1, 2, 2);
		ret.put(1, 3, 4);
		ret.put(2, 3, 1);
		ret.put(2, 4, 7);
		ret.put(3, 4, 3);
		return ret;
	}

}
