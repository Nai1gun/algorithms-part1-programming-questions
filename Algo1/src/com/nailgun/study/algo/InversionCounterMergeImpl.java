package com.nailgun.study.algo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class InversionCounterMergeImpl implements InversionCounter{

	@Override
	public long count(List<Integer> list) {
		
		long result = 0l;
		
		int numLists = list.size();
		List<List<Integer>> sublists1 = new ArrayList<List<Integer>>(numLists);
		for (int i: list){
			sublists1.add(Arrays.asList(i));
		}
		while (true){
			Iterator<List<Integer>> sublistsIter = sublists1.iterator();
			List<List<Integer>> sublists2 = new ArrayList<List<Integer>>();
			while (sublistsIter.hasNext()){
				List<Integer> firstList = sublistsIter.next();
				if (sublistsIter.hasNext()){
					List<Integer> secondList = sublistsIter.next();
					int firstSize = firstList.size();
					int secondSize = secondList.size();
					int i = 0, j = 0;
					List<Integer> mergedList = new ArrayList<Integer>(firstSize + secondSize);
					while (true){
						int first = firstList.get(i);
						int second = secondList.get(j);
						if (first < second){
							mergedList.add(first);
							i++;
						} else {
							mergedList.add(second);
							j++;
							result += firstSize - i;
						}
						if (j == secondSize){
							if (i < firstSize - 1){
								result += (firstSize - 1 - i) * (secondSize - j);
							}
							while (i < firstSize){
								mergedList.add(firstList.get(i));
								i++;
							}
							break;
						} else if (i == firstSize) {
							while (j < secondSize){
								mergedList.add(secondList.get(j));
								j++;
							}
							break;
						}
					}
					sublists2.add(mergedList);					
				} else {
					sublists2.add(firstList);
				}
			}	
		
		if (sublists2.size() == 1) break;
		sublists1 = sublists2;
	}
	
	return result;
	}

}
