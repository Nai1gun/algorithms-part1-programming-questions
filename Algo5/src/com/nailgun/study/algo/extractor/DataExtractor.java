package com.nailgun.study.algo.extractor;

import com.google.common.collect.Table;

public interface DataExtractor {
	
	Table<Integer, Integer, Integer> extract();
}
