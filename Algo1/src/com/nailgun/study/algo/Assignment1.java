package com.nailgun.study.algo;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.perf4j.StopWatch;

public class Assignment1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		InputStream arrayStream = Assignment1.class.getResourceAsStream("IntegerArray.txt");
		try {
			List<String> strLines = IOUtils.readLines(arrayStream);
			System.out.println("Lines size: " + strLines.size());
			List<Integer> ints = new ArrayList<Integer>(strLines.size());
			for (String str: strLines){
				int number = Integer.parseInt(str);
				ints.add(number);
			}
			//List<Integer> ints = Arrays.asList(8, 6, 7, 1, 4, 3, 2, 5);
			//InversionCounter counter = new InversionCounterStupidImpl();
			InversionCounter counter = new InversionCounterMergeImpl();
			StopWatch sw = new StopWatch("inversions count");
			sw.start();
			System.out.println("Inversions count: " + counter.count(ints));
			sw.stop();
			System.out.println("Count time: " + sw.getElapsedTime());
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			IOUtils.closeQuietly(arrayStream);
		}
		
	}

}
