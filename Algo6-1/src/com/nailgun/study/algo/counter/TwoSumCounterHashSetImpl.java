package com.nailgun.study.algo.counter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TwoSumCounterHashSetImpl implements TwoSumCounter{
	
	private final Set<Integer> set;
	
	public TwoSumCounterHashSetImpl(List<Integer> list){
		set = new HashSet<Integer>(list);
	}

	@Override
	public int count(Integer t) {
		int ret = 0;
		for (Integer i: set){
			if (t != i*2 && set.contains(t-i)) ret++;
		}
		return ret;
	}

}
